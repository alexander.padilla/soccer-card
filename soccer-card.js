import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-styles/paper-styles.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/social-icons.js';
import '@polymer/iron-image/iron-image.js';

/**
 * `soccer-card`
 * Soccer details about a player
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class SoccerCard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          --iron-icon-width: 20px;
          --iron-icon-height: 20px;
        }
        
        .horizontal {
          @apply --layout-horizontal;
        }
        
        .vertical {
          @apply --layout-vertical;
        }
        
        .center {
          @apply --layout-center-justified;
        }
        
        .justify {
          @apply --layout-justified;
        }
        
        .cover {
          width: 200px;
          object-fit: cover;
        }
        
        .header {
          min-height: 75px;
        }
        
        .orders {
          color: var(--paper-grey-500);
          @apply --paper-font-body2;
        }
        
        .card-footer {
          @apply --layout-center-justified;
          @apply --layout-horizontal;
        }
        
        .buton-edit {
          color: var(--paper-indigo-800);
        }
        
        paper-card {
          width: 30%;
          margin: 16px;
          padding: 16px;
          --paper-card-header-image:{
            height: 150px;
            margin-bottom: 10px;
          };
        }
        
        .title {
          @apply --paper-font-title;
          color: var(--paper-grey-900);
          width: 90%;
          white-space: normal;
        }
        
        .data {
          @apply --paper-font-subhead;
          color: var(--paper-grey-700);
        }
        
        .description {
          color: var(--paper-grey-500);
          @apply --paper-font-body2;
          min-height: 50px;
        }
        
        .text-justify {
          text-align: justify;
        }
        
        .text-center {
          text-align: center;
        }
        
        iron-image {
          width: 50px;
          height: 50px;
        }
        
        .margin {
          margin: 5px;
        }
        
        .margin-t {
          margin-top: 6px;
        }
        
      </style>
      
      <paper-card image="[[image]]" class="cover">
        <div>
          <div class="horizontal text-center">
            <span class="title">[[name]]</span>
          </div>
          <div class="horizontal justify">
            <div class="vertical">
              <div class="horizontal">
                <iron-icon class="margin-t" src="https://i.imgur.com/YdElEZo.png"></iron-icon>
                <span class="data margin">[[position]]</span>
              </div>
              <div class="horizontal">
                <iron-icon class="margin-t" src="https://i.imgur.com/0ibMCcL.png"></iron-icon>
                <span class="data margin">[[goals]]</span>
              </div>
            </div>
            <div class="vertical">
              <iron-image src="[[team_img]]" sizing="cover"></iron-image>
            </div>
          </div>
        </div>
        <div>
          <div class="card-footer">
            <paper-button class="buton-edit" on-click="_onEditEvent">Ver mas</paper-button>
          </div>
        </div>
      </paper-card>
    `;
  }
  
  static get properties() {
    return {
      image: {
        type: String,
        value: 'https://i.imgur.com/IYlX60y.png'
      },
      name: {
        type: String,
        value: 'Nombre Apellido'
      },
      position: {
        type: String,
        value: 'DEF'
      },
      team: {
        type: String,
        value: ''
      },
      team_img: {
        type: String,
        value: 'https://i.imgur.com/sDsLwAP.png'
      },
      goals: {
        type: Number,
        value: 2
      },
      id: {
        type: String,
        value: '-1'
      }
    };
  }
  
  ready(){
    super.ready();
  }
  
  _onEditEvent(){
    this.dispatchEvent(new CustomEvent('click', {detail: {id: this.id}}));
  }
}

window.customElements.define('soccer-card', SoccerCard);
